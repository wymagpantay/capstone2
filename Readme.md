E-COMMERCE API DOCUMENTATION

TEST ACCOUNTS:
- Regular Users:
     - email: test@mail.com
     - pwd: test1234

     - email: wymagpantay@gmail.
     - pwd: wyman1234	

- Admin User:
    - email: admin@mail.com
    - pwd: admin1234
    

ROUTES:

- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - email (string)
        - password (string)

- User Authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)

- Retrieve All Products (GET)
	- http://localhost:4000/products/all
    - request body: none]

- Retrieve all Active Products (GET)
	- http://localhost:4000/products
    - request body: none

- Search for a Specific Product (GET)
	- http://localhost:4000/products/:id
    - request body: none
	
- Retrive User Details (POST)
	- http://localhost:4000/users/details
    - request body: 
        - email (string)
        - password (string)

- Create Product (Admin Only) (POST)
	- http://localhost:4000/products/create-product
    - request body: 
        - name (string)
        - description (string)
        - price (number)

- Edit Product (Admin Only) (PUT)
	- http://localhost:4000/products/:id
    - request body: 
        - name (string)
        - description (string)
        - price (number)

- Archive a Specific Product (Admin Only) (PUT)
	- http://localhost:4000/products/:id/archive
    - request body: none

- Activate a Specific Product (Admin Only) (PUT)
	- http://localhost:4000/products/:id/activate
    - request body: none

- Create Order (User) (POST)
	- http://localhost:4000/orders/userId/create-order
    - request body: 
        - products (array)

- Change Admin Access (PUT)
	- http://localhost:4000/users/:id/admin-access
    - request body: none

- Get All Orders of Logged In User (Must be logged in) (GET)
	- http://localhost:4000/orders
    - request body: none

- Get All Existing Orders (GET)
	- http://localhost:4000/orders/all
    - request body: none

- Delete Specific Order (Must be logged in) (DELETE)
	- http://localhost:4000/orders/:id
    - request body: none
