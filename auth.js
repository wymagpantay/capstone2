const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI";


module.exports.createAccessToken = (user)=>{
	
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}


module.exports.verify = (req,res,next)=>{
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth:"Failed. No Token"});
	} else {
		console.log("With bearer prefix")
		console.log(token);
		token = token.slice(7,token.length);
		console.log("No bearer prefix")
		console.log(token);

		jwt.verify(token, secret, function(err,decodedToken){
			if(err){
				return res.send({
					auth:"Failed",
					message: err.message
				})
			} else {
				console.log("Data that will be assigned to  the req.user")
				console.log(decodedToken)
				req.user = decodedToken
				next()
			}
		})
	}
}

module.exports.verifyAdmin = (req,res,next)=>{
	// verifyAdmin comes after the verify middleware
	if(req.user.isAdmin){
		next()
	} else {
		return res.send ({
			auth:"Failed",
			message:"Action Forbidden"
		})
	}
}