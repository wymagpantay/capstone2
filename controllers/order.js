const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const mongoose = require('mongoose')

// Create Order for a User
module.exports.createOrder = (req, res) => {
    const { products } = req.body;
    const userId = req.user.id;

    User.findById(userId)
        .then(user => {
            if (!user || user.isAdmin) {
                return res.status(400).json({ error: "Invalid user or admin" });
            }
            Product.find({ _id: { $in: products.map(product => product.productId) } })
                .then(foundProducts => {
                    const totalAmount = products.reduce((total, product) => {
                        const foundProduct = foundProducts.find(p => p._id.toString() === product.productId);
                        if (!foundProduct || typeof product.quantity !== "number") {
                            console.log(`Invalid product data: ${product.productId}`);
                            return total;
                        }
                        return total + foundProduct.price * product.quantity;
                    }, 0);
                    const newOrder = new Order({
                        userId: userId,
                        products: products,
                        totalAmount: totalAmount
                    });
                    newOrder.save()
                        .then(order => {
                            return res.status(201).json(order);
                        })
                        .catch(error => {
                            return res.status(500).json({ error: "Failed to save order" });
                        });
                })
                .catch(error => {
                    return res.status(500).json({ error: "Failed to fetch product prices" });
                });
        })
        .catch(error => {
            return res.status(500).json({ error: "Failed to fetch user" });
        });
};


// Get all orders for verified User
module.exports.getOrders= (req, res) => {
  const userId = req.user.id;
  Order.find({ userId: userId })
    .then((orders) => {
      res.json(orders);
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ error: "Failed to retrieve orders" });
    });
};


// Get All Orders (Admin Only)
module.exports.getAllOrders = (req,res)=>{
    return Order.find({}).then(result=>{
        return res.send(result)
    })
}

// Delete Order
module.exports.deleteOrder = (req, res) => {
    const orderId = req.params.id;
    Order.findByIdAndRemove(orderId)
        .then((removedOrder) => {
            if (!removedOrder) {
                return res.status(404).json({ message: "Order not found" });
            }
            return res.status(200).json({ message: "Order has been deleted" });
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({ message: "An error occurred while deleting the order" });
        });
};
