const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Register a User
module.exports.registerUser = (reqbody) =>{
	let newUser = new User({
		email:reqbody.email,
		password: bcrypt.hashSync(reqbody.password,10)
	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false
		}else{
			return true
		}

	})
	.catch(err=>err)
}


// Login as a User
module.exports.loginUser = (req,res)=>{
	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}


// Create Order for a User
module.exports.createOrder = (req, res) => {
    const { products } = req.body;
    const userId = req.user.id; 
    User.findById(userId)
        .then(user => {
            if (!user || user.isAdmin) {
                return res.send(false);
            }
            const totalAmount = products.reduce((total, product) => {
                return total + product.price * product.quantity;
            }, 0);
            const newOrder = new Order({
                userId: userId,
                products: products,
                totalAmount: totalAmount
            });
            newOrder.save()
                .then(order => {
                    return res.send(order);
                })
                .catch(error => {
                    return res.send(false);
                });
        })
        .catch(error => {
            return res.send(false);
        });
};


// Search for specific User
module.exports.getProfile = (req, res) => {
    return User.findById(req.user.id)
    .then(result => {
        result.password = "";
        return res.send(result);
    })
    .catch(err => res.send(err))
};


module.exports.updateAdminAccess = (userId, newStatus) => {
  return User.findById(userId).then((result) => {
    if (!result.isAdmin) {
      result.isAdmin = true;
    } else {
      result.isAdmin = false;
    }
    return result.save().then((updatedUser, saveErr) => {
      if (saveErr) {
        console.log(saveErr);
        return "Failed to update admin access";
      } else {
        return "The user's admin access has been successfully updated";
      }
    });
  });
};