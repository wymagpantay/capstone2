const express = require("express");
const userController = require("../controllers/user");
const productController = require("../controllers/product");
const orderController = require("../controllers/order");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create Order for a User
router.post("/:userId/create-order", verify, orderController.createOrder);

// Get all orders for verified User
router.get('/',verify, orderController.getOrders);

// Get All Orders (Admin Only)
router.get("/all",verify, verifyAdmin, orderController.getAllOrders);

// Delete Order
router.delete("/:id",verify,orderController.deleteOrder);

module.exports = router;