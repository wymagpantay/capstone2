const express = require("express");
const productController = require("../controllers/product");
const orderController = require("../controllers/order");
const userController = require("../controllers/user");
const auth = require("../auth") 
const {verify, verifyAdmin} = auth;

const router = express.Router();


//Create a Product (Verify Admin)
router.post("/create-product", verify, verifyAdmin, productController.addProduct)

// Get All Products
router.get("/all",productController.getAllProducts);

// Get All (Active) Products
router.get("/",productController.getAllActiveProducts);

// Get a Specific Product
router.get("/:productId",productController.getProduct);

// Edit a Product (Verify Admin)
router.put("/:productId",verify, verifyAdmin, productController.updateProduct);

//Archive a Product (Admin Only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

//Activate a Product (Admin)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

module.exports = router;