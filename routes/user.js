const express = require("express");
const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const router = express.Router();

// Register a User
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Login as a User
router.post("/login",userController.loginUser);

// Search for specific User
router.post("/details", verify, userController.getProfile);

// Update Admin Access
router.put("/:id/admin-access",(req,res)=>{
	userController.updateAdminAccess(req.params.id, req.body.isAdmin).then(resultFromController=>res.send(resultFromController));
});



module.exports = router